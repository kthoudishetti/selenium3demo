package demoscripts;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

import org.testng.annotations.Test;

public class ReadWritePropertyFile {

	@Test (priority=1)	
	public static void setConfigFile() {
		Properties prop = new Properties();
		OutputStream output = null;
		try {
			output = new FileOutputStream("./src/test/java/demoscripts/config.properties");
			// load a properties file
			prop.setProperty("database", "localhost");
			prop.setProperty("dbuser", "mkyong");
			prop.setProperty("dbpassword", "password");
			
			prop.store(output, null);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test (priority=2)	
	public static void redConfigFile() {
		Properties prop = new Properties();
		InputStream input = null;
		try {
			input = new FileInputStream("./src/test/java/demoscripts/config.properties");
			// load a properties file
			prop.load(input);
			// get the property value and print it out
			System.out.println(prop.getProperty("database"));
			System.out.println(prop.getProperty("dbuser"));
			System.out.println(prop.getProperty("dbpassword"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
