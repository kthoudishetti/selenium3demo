package demoscripts;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.testng.annotations.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ReadWriteXMLFile {
	/*
	 * We have 3 different XML Parsers to access XML files
	 * 1. DOM XML Parser
	 *  - The DOM is the easiest to use Java XML Parser. 
	 *    It parses an entire XML document and load it into memory, modeling it with Object for easy nodel traversal. 
	 *    DOM Parser is slow and consume a lot memory if it load a XML document which contains a lot of data.
	 * 2. SAX XML Parser
	 *  - SAX parser is work differently with DOM parser, it does not load any XML document into memory and create some object representation of the XML document. Instead, 
	 *    the SAX parser use callback function (org.xml.sax.helpers.DefaultHandler) to informs clients of the XML document structure.
	 * 3. JDOM XML Parser
	 *  - JDOM provides a way to represent that document for easy and efficient reading, manipulation, and writing. 
	 *    It�s an alternative to DOM and SAX.
	 * */

	@Test
	public void ReadXML_DOMXMlParser() {
		try {

			File fXmlFile = new File("./src/test/java/demoscripts/testData.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
					
			//optional, but recommended
			//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
			//doc.getDocumentElement().normalize();
			
			System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
					
			NodeList nList = doc.getElementsByTagName("staff");
					
			System.out.println("----------------------------");

			for (int temp = 0; temp < nList.getLength(); temp++) {

				Node nNode = nList.item(temp);
						
				System.out.println("\nCurrent Element :" + nNode.getNodeName());
						
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {

					Element eElement = (Element) nNode;

					System.out.println("Staff id : " + eElement.getAttribute("id"));
					System.out.println("First Name : " + eElement.getElementsByTagName("firstname").item(0).getTextContent());
					System.out.println("Last Name : " + eElement.getElementsByTagName("lastname").item(0).getTextContent());
					System.out.println("Nick Name : " + eElement.getElementsByTagName("nickname").item(0).getTextContent());
					System.out.println("Salary : " + eElement.getElementsByTagName("salary").item(0).getTextContent());

				}
			}
		    } catch (Exception e) {
			e.printStackTrace();
		    }
	}
	
	@Test
	public void WriteXML_DOMXMlParser() {
		
	}
}
