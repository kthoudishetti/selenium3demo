package demoscripts;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;


public class RunnerTests {
	public WebDriver driver = new FirefoxDriver();
	
	       String appUrl = "https://gmail.com/";
	       
	@Test
	public void validateTextOnJQuerySite() throws InterruptedException {
	
	              driver.manage().window().maximize();
	              driver.get(appUrl);
	              JavascriptExecutor js=(JavascriptExecutor)driver;
	              
	             /* getText Using Jquery
	              * String appURL = "https://jquery.com/";
      	            Assert.assertEquals(js.executeScript("return $('.icon-wrench').text();"), "API", "Failure");
      	          */
	              
	             /* set background color to 'yellow' for all the elements including html, head and body tags
	              * js.executeScript("return $(\"*\").css(\"background-color\",\"yellow\");");
	             */
	              
	              /*
	               * set background color to 'yellow' for all Div elements
	                 js.executeScript("return $(\"*div\").css(\"background-color\",\"yellow\");");
	              */
	              
	               //Enter text into the input fields using Javascript
	                  String stringToEnterIntoTextField = "NewValue";
	                  //WebElement inputField = driver.findElement(By.id("identifierId"));
	                  //inputField.sendKeys("hahahahaa");
	                  
					  js.executeScript("return $(\"[name='identifier']\").value='sdsdsd';");
					  Thread.sleep(10000);
	                 
	             //  2. Select all tags in the DOM including html, head and body - js.executeScript("return $(("*").css("background-color", "yellow"))");
	              // 3. Select all tags start with div - driver.executeScript("return $(("*div").css("background-color", "yellow"))");
	           
	              driver.close();
	}

}
